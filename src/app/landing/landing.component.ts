import {
  Component,
  OnInit
} from '@angular/core';
import {
  CoonnetorService
} from '../services/coonnetor.service';
import {
  MessageService
} from '../services/message.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  validAdd: boolean = true;

  constructor(private coonnetorService: CoonnetorService, private messageService: MessageService) {


  }

  ngOnInit() {

  }

  edit(idtodo) {

    this.messageService.filtr.forEach(v => (v.id == idtodo) ? v.isEdit = !v.isEdit : v.isEdit = false)
  }
  patchTodo(data, tid) {
    this.coonnetorService.updateTodos(tid, data).subscribe((todos) => {
      console.log(todos);
    });
  }

  updateName(name, idtodo) {
    this.messageService.filtr.forEach(v => {
      if (v.id == idtodo) {
        if (name != "") {
          v.valid = true;
          v.name = name;
          v.isEdit = !v.isEdit;

          this.coonnetorService.updateNameToDoList(idtodo, name).subscribe(sub => {

          })
        } else {
          v.valid = false;
        }
      }
    })


  }
  cancel() {

  }
  del(id) {
    this.coonnetorService.deleteToDoList(id).subscribe(res => {
      if (res.ok) {
        this.messageService.todolist.forEach((el, i) => {
          if (el.id == id) {
            this.messageService.todolist.splice(i, 1);
          }
        });
      }
    });
  }
  find(f) {
    this.messageService.filtr = this.messageService.todolist.filter(v => {
      return v.name.toLowerCase().match(f.toLowerCase())
    });
  }
  active(id) {

    this.messageService.filtr.forEach(el => {
      if (id == el.id) {
        el.isActive = true;
        this.messageService.nameList = el.name;
      } else {
        el.isActive = false;
      }
    });
  }
}

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CoonnetorService {

  url: string = "https://todos.venturedevs.net/api/";

  constructor(public http: Http) {
    console.log("data service connet");
   }
    headers = new Headers({ 'Content-Type': 'application/json' });
    options = new RequestOptions({ headers: this.headers });

  //main
   getToDoList() {
    return this.http.get(this.url + "todolists/?format=json")
      .map(res => res.json());
  }
  createToDoList(todolists){
    return this.http.post(this.url + "todolists/?format=json",todolists, this.options)
    .map(res => res.json());
  }

  updateNameToDoList(id, name){
    return this.http.patch(this.url + "todolists/"+id+"/?format=json",{"name": name}, this.options)
    .map(res => res.json());
  }
  deleteToDoList(id){
    return this.http.delete(this.url + "todolists/"+id)
    
  }
//todos
  getTodos(id:number) {
    return this.http.get(this.url + "todolists/"+id+"/?format=json")
      .map(res => res.json());
  }
  updateTodos(id:number, data){
    return this.http.patch(this.url + "todos/"+id+"/?format=json",data, this.options)
    .map(res => res.json());
  }
  addtodo( data){
    return this.http.post(this.url + "todos/?format=json",data, this.options)
    .map(res => res.json());
  }
  deltodo( id){
    return this.http.delete(this.url + "todos/"+id)
  }
  
}

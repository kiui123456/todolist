import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToDoListsComponent } from './to-do-lists/to-do-lists.component';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [ {

  path: "",
  component: LandingComponent
},{

  path: "todolist/:id",
  component: ToDoListsComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

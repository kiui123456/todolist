import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  QueryList,
  ViewChildren
} from '@angular/core';
import {
  ActivatedRoute
} from '@angular/router';
import {
  CoonnetorService
} from '../services/coonnetor.service';
import {
  MessageService
} from '../services/message.service';
import {
  Observable
} from 'rxjs/Observable';

import 'rxjs/add/observable/of';

@Component({
  selector: 'app-to-do-lists',
  templateUrl: './to-do-lists.component.html',
  styleUrls: ['./to-do-lists.component.scss']
})
export class ToDoListsComponent  implements OnInit {

  filtr: any = [];
  todos: any;
  isEdit: boolean = false;
  isEditName: boolean = false;
  validAdd:boolean = true;
  validName:boolean = true;
  private id: number;
  nameToDoList: string;


  constructor(private route: ActivatedRoute, private coonnetorService: CoonnetorService, private messageService: MessageService) {

  }

  ngOnInit() {

    this.route.params.subscribe(res => {
      this.id = res.id;
      this.messageService.lid = res.id;
      this.coonnetorService.getTodos(res.id).subscribe((todos) => {
        todos.forEach(v => {
          v.isEdit = false
          v.valid = true;
        });
        this.todos = todos;
        this.filtr = todos;
      });

    })


  }




  patchTodo(data, tid) {
    this.coonnetorService.updateTodos(tid, data).subscribe((todos) => {
      console.log(todos);
    });
  }
  change(e, tid) {
    let compl = 1;
    this.patchTodo({
      "is_complete": e.target.checked
    }, tid);
    this.todos.forEach(todos => {
      (todos.id == tid) ? todos.is_complete = !todos.is_complete: '';
      (todos.is_complete) ? compl++ : "";

    })

  }

  edit(idtodo) {

    this.todos.forEach(v => (v.id == idtodo) ? v.isEdit = !v.isEdit : v.isEdit = false)
  }
  updateName(name, idtodo) {

    this.todos.forEach(v => {
      if (v.id == idtodo) {
        if (name != "") {
          v.name = name;
          v.isEdit = !v.isEdit;
          v.valid = true;
          this.patchTodo({
            "name": name
          }, idtodo);
        } else {
          v.valid = false;
        }
      } else {
        v.valid = true;
      }
    })


  }
  add(name, compl) {
    if (name.value != "") {
      this.validAdd = true;
      this.coonnetorService.addtodo({
        "name": name.value,
        "is_complete": compl.checked,
        "todo_list": this.id
      }).subscribe((todos) => {
        todos.valid = true;
        this.filtr.push(todos)
        this.messageService.todolist.forEach(v => {
          if (this.id == v.id) {
            v.todos_count++;
          }
        })
      })
    }else{
      this.validAdd = false;
    }
  }

  del(id) {
    this.coonnetorService.deltodo(id).subscribe(res => {

      if (res.ok) {
        this.filtr.forEach((el, i) => {
          if (el.id == id) {
            this.filtr.splice(i, 1);
          }
        });
        this.messageService.todolist.forEach(v => {
          if (this.id == v.id) {
            v.todos_count--;
          }
        })
      }
    });
  }
  
  editName() {
    
    this.isEditName = !this.isEditName;
  }
  rename(listName) {

    if (listName.value != "") {
      this.editName();
      this.validName = true;
      this.coonnetorService.updateNameToDoList(this.id, listName.value).subscribe(sub => {
        this.messageService.todolist.forEach(el => {
          if (el.id == this.id) {
            el.name = this.messageService.nameList = listName.value;

          }
        });
      })

    }else{
      this.validName= false;
    }
  }
}

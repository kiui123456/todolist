import {
  Component,
  Input,
  Output
} from '@angular/core';
import {
  CoonnetorService
} from './services/coonnetor.service';
import {
  MessageService
} from './services/message.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],


})
export class AppComponent {

  todolist: any;
  isEdit: boolean = false;
  validAdd:boolean = true;
  constructor(private route: ActivatedRoute, private coonnetorService: CoonnetorService, private messageService: MessageService) {
    this.coonnetorService.getToDoList().subscribe((todolist) => {
     
      todolist.forEach(el=> {
        el.valid = true;
        if (this.messageService.lid == el.id){
          this.messageService.nameList = el.name;
            el.isActive = true;
        }else{
          el.isActive = false;
        }  
    })
        this.messageService.todolist = todolist;
        this.messageService.filtr = todolist;
        
       

        //  console.log(this.ApartamentsPool[i].description);
      }

    );

  }



  edit() {
    this.isEdit = !this.isEdit
  }
  add(name: string) {
    if(name != "")
    {
      this.validAdd = true;
    this.coonnetorService.createToDoList({
      "name": name
    }).subscribe((res) => {
      console.log(res)
      this.messageService.todolist.push(res);
    //  this.filtr.push(res);
    })
    this.edit();
  }else{
    this.validAdd = false;
  }

  }
  cancel() {
    this.edit();
  }
  del(id) {
    this.coonnetorService.deleteToDoList(id).subscribe(res => {
      if (res.ok) {
        this.messageService.todolist.forEach((el, i) => {
          if (el.id == id) {
            this.messageService.todolist.splice(i, 1);
          }
        });
      }
    });
  }
  find(f){
  this.messageService.filtr = this.messageService.todolist.filter(v=>{
   return v.name.toLowerCase().match(f.toLowerCase())
  });
  }
  active(id){
    
    this.messageService.filtr.forEach(el => {
      if(id == el.id){
        el.isActive = true;
        this.messageService.nameList = el.name;
      }else{
        el.isActive = false;
      }
    });
  }

}
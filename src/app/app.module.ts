import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ToDoListsComponent } from './to-do-lists/to-do-lists.component';

import { CoonnetorService } from './services/coonnetor.service';
import { MessageService } from './services/message.service';
import { LandingComponent } from './landing/landing.component';

@NgModule({
  declarations: [
    AppComponent,
    ToDoListsComponent,
    LandingComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [CoonnetorService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
